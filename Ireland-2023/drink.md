# Listed in distance (closest to farest) from the hotel

## Pub
- [The Brazen Head](http://www.brazenhead.com/)
- [Temple Bar]

## Brewery
- [Guinness Open Gate](https://www.guinnessopengate.com/)
- [The Guinness Storehouse](https://www.guinness-storehouse.com/en/Index.aspx)

## Distillery
- [Jameson](https://www.jamesonwhiskey.com/ie/visit-us/jameson-distillery-bow-st)
- [Teeling](https://teelingdistillery.com/tasting-tours/#tours)

