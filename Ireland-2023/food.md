# Listed in distance (closest to farest) from the hotel

## All

- [Merchant's Arch](http://www.merchantsarch.ie/)

## Breakfast & Lunch & Brunch
- [Keoghscafe](http://keoghscafe.ie/)
- [Brother Hubbard North](http://www.brotherhubbard.ie/)
- Lovinspoon

## Brunch

- [Brother Hubbard (North)](https://brotherhubbard.ie)

## Lunch & Dinner
- [The Salmon Leap Inn](https://salmonleapinn.com/)
   - Steak
   - Fish
- Leo Burdock
   - Fish
- [PHX Bistro](https://www.phxbistro.com/)
   - Lamb

## Desserts
- [Murphys Ice Cream](https://murphysicecream.ie/Murphys_Ice_Cream_Dublin_Shops.html)
