# Mykaela and I's trip to Ireland

![Area of Hotel](hotel-area.png)

## [Sightseeing](https://gitlab.com/ahoneybun/international-travel/-/blob/main/Ireland-2023/sightseeing.md)
## [Food](https://gitlab.com/ahoneybun/international-travel/-/blob/main/Ireland-2023/food.md)
## [Drink](https://gitlab.com/ahoneybun/international-travel/-/blob/main/Ireland-2023/drink.md)

## [Plan](https://gitlab.com/ahoneybun/international-travel/-/blob/main/Ireland-2023/planning.md)
