# 

## Historical landmark
- [The Wonderful Barn](http://kildare.ie/heritage/historic-sites/wonderful-barn.asp)
- [Castletown House](http://www.castletown.ie/)
- [Phoenix Park](http://www.phoenixpark.ie/)

## Misc
- [x] [Dublin Zoo](https://www.dublinzoo.ie/)
- [St Stephen's Green](https://heritageireland.ie/visit/places-to-visit/st-stephens-green/)
- Meeting House Square

## Points of Interest & Landmarks
- [Grafton Street]
- [Merrion Square]

## Tourist attraction
- [Farmleigh House & Estate](https://farmleigh.ie/)

## Village
- [Howth Head]

## Museum
- 14 Henrietta Street
- [EPIC The Irish Emigration Museum](https://epicchq.com/)
- [National Museum of Ireland](https://www.museum.ie/Home)
- [National Museum of Ireland – Archaeology](https://www.museum.ie/Home)
- [x] Chester Beatty

## Cemetery
- Glasnevin Cemetery

## Castle
- [x] [Dublin Castle](http://www.dublincastle.ie/)
- [Closed] [Drimnagh Castle](http://www.drimnaghcastle.org/)
- [Closed] [Howth Castle](http://howthcastle.ie/)

## Art Gallery
- [National Gallery of Ireland](https://www.nationalgallery.ie/)
- [Newgrange](https://www.newgrange.com/)
- [St Patrick's and Christ Church Cathedrals](https://www.stpatrickscathedral.ie/)
- [x] Ha'penny Bridge
